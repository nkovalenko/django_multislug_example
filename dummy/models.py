from django.db import models


class Direction(models.Model):
    title = models.CharField(max_length=100)
    slug = models.CharField(max_length=256, unique=True)


class Location(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField(max_length=500, blank=True)
    seo_title = models.TextField(max_length=500, blank=True)
    directs = models.ForeignKey(Direction, on_delete=models.PROTECT)
    slug = models.CharField(max_length=256, unique=True)


class Property(models.Model):
    title = models.CharField(max_length=255)
    catalog = models.ForeignKey(Location, on_delete=models.PROTECT)
    time_create = models.DateTimeField(auto_now_add=True)
    time_update = models.DateTimeField(auto_now=True)
    is_published = models.BooleanField(default=True)
    slug = models.CharField(max_length=256, unique=True)
