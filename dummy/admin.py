from django.contrib import admin

from .models import *

admin.site.register(Direction)
admin.site.register(Location)
admin.site.register(Property)
