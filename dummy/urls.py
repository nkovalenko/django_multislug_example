from django.urls import path

from . import views

urlpatterns = [
    path('', views.directions),
    path('<str:direction>/', views.locations),
    path('<str:direction>/<str:location>/', views.properties),
    path(
        '<str:direction>/<str:location>/<str:property>/',
        views.property_detail,
    ),
]
