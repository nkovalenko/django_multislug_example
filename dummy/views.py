from django.core import serializers
from django.http import JsonResponse

from dummy.models import Direction, Location, Property


def directions(request):
    objects = Direction.objects.all()

    return JsonResponse(serializers.serialize('json', objects), safe=False)


def locations(request, direction):
    objects = Location.objects.filter(directs__slug=direction)

    return JsonResponse(serializers.serialize('json', objects), safe=False)


def properties(request, location, **_):
    objects = Property.objects.filter(catalog__slug=location)

    return JsonResponse(serializers.serialize('json', objects), safe=False)


def property_detail(request, property, **_):
    obj = Property.objects.get(slug=property)

    return JsonResponse(serializers.serialize('json', [obj,]), safe=False)