FROM python:3.10

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /opt

COPY requirements.txt /opt
RUN pip install -r requirements.txt

COPY . /opt/
